import numpy as np
import pandas as pd
import sys
import shlex
import os, errno

pd.options.mode.chained_assignment = None

def usage():
    print "This python script separates a csv into two: first occurrences and duplicates, based on a given field."
    print "First occurrences are determined by the order of the rows of the original file."
    print "Reference file must be given to determine which entries are new."
    print "Set flag -c for categorize: generates different files for each value of the split field. Use with"
    print "caution."
    print "If no field is given, spreadsheet will just be cleaned."
    print
    print "Usage:"
    print "|   python CleanSplit.py <ref file path> <input file path> 'split field'"
    print "|   python CleanSplit.py <ref file path> <input file path> 'split field' -c"
    print "|   python CleanSplit.py <ref file path> <input file path>"
    print "Field names MUST be surrounded by quotes!"
    print "Reference file path may be replaced by '.' for new project or overview."
    print
    print "Ex:"
    print "|    python CleanSplit.py ref.csv main.csv 'field name'"
    print "|    python CleanSplit.py . main.csv 'field name'"
    print "Ordinarily outputs to singleton.csv and duplicates.csv."
    print "Categorical outputs to csvs named for each value."
    print "Clean pass outputs to main-clean.csv."
    print "Outputs to folder the script is run from."
    return

# Pull in various configuration settings

def get_distinguished():
    dist = {
        "Unique": [],
        "Date": [],
        "Phone": [],
        "Populate": [],
        "String": []
        }
    try:
        with open('dist.conf', 'r') as d:
            for line in d:
                llist = line.strip().split(';')
                dist[llist[0].strip()] = [x.strip() for x in llist[1].split(',')]
        return dist
    except:
        raise Exception("No configuration file found! (dist.conf)")
    return

def get_corrections():
    corrections = {}
    label = ""
    try:
        with open('corrections.conf','r') as c:
            for line in c:
                llist = line.strip().split(';')
                if len(llist) == 1:
                    label = llist[0]
                    corrections[label] = {}
                else:
                    corrections[label][llist[0]] = llist[1].strip()
        return corrections
    except:
        raise Exception("No configuration file found! (corrections.conf)")
        return

# Define cleanup based on corrections.conf

def df_correct(df,field,initial,swap):
    df[field].replace(initial,swap,inplace=True)
    return df        

def total_correct(df, field, argdict):
    for key,value in argdict.items():
        df = df_correct(df,field,key,value)
    return df

def iterated_correct(df, corrections):
    for key,value in corrections.items():
        df = total_correct(df, key, value)
    return df

def cleansplit(act_args,dist,corr):
    # Check if we've been given what we need to run
    if len(act_args) != 3 and len(act_args) != 4 and len(act_args) != 5 and len(act_args) != 6:
        usage()
        return
    try:
        toparse = act_args[3]
    except:
        toparse = 'clean'

    # Create reference dataframe from ref file
    if act_args[1] != ".":
        rdf = pd.read_csv(act_args[1])
        rlines = rdf.shape[0]
        refmod = "(trimmed)"
    else:
        rlines = 0
        refmod = ""
        
    # Create initial dataframe from file
    df = pd.read_csv(act_args[2])

    # Make a place for our results
    outpath = act_args[2][:-4]
    try:
        os.makedirs(outpath + '\\' + toparse)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    
    # Remove any 'ghost columns'
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

    # Remove empty rows
    df.dropna(axis=0, how='all', inplace=True)

    # Remove surrounding whitespace in string type columns
    if dist["String"]:
        for col in dist["String"]:
            df[col].fillna('', inplace=True)
            for index,row in df.iterrows():
                f_val = str(row[col]).strip()
                df.loc[index,col] = f_val

    if dist["Populate"] and dist["Unique"]:
        # Populate across matching records with latest entry
        for identity, data in df.groupby(dist["Unique"][0]):
            if identity:
                for item in dist["Populate"]:
                    if data[data[item].notnull()][item].any():
                        df.loc[df[dist["Unique"][0]] == identity,item] = data[data[item].notnull()][item].sort_values(ascending=False).iloc[0]

    # Now we can chop off old records
    df = df[rlines:]

    if dist["Date"]:
        # Check if date column is working correctly, fix it if not
        try:
            df[dist["Date"][0]] = pd.to_datetime(df[dist["Date"][0]])
        except:
            # Remove NaNs in dates
            df[dist["Date"][0]].fillna(0, inplace=True)

            # Expand year in date column
            for index, row in df.iterrows():
                if row[dist["Date"][0]] != 0:
                    if len(row[dist["Date"][0]].split('/')[2]) != 4:
                        row[dist["Date"][0]] = row[dist["Date"][0]][:-2] + "20" + row[dist["Date"][0]][-2:]
                    row[dist["Date"][0]] = pd.to_datetime(row[dist["Date"][0]], format='%m/%d/%Y', errors='coerce')

            # Make zero dates into NaT
            df[dist["Date"][0]].replace(0, pd.NaT, inplace=True)

        # Finally, we can sort by date.
        df.sort_values(by=dist["Date"][0], ascending=False, inplace=True)

    if dist["Phone"]:
        # Make phone numbers play nice with Excel
        df[dist["Phone"][0]].fillna(" ", inplace=True)
        for index, row in df.iterrows():
            f_row = "".join(c for c in row[dist["Phone"][0]] if c not in [' ','(',')','-'])
            if len(f_row) <= 10:
                df.loc[index,dist["Phone"][0]] = f_row[:3]+'-'+f_row[3:6]+'-'+f_row[6:]
            elif len(f_row) > 10 and "Int:" not in f_row:
                df.loc[index,dist["Phone"][0]] = 'Int: +'+f_row[:-10]+'-'+f_row[-10:-7]+'-'+f_row[-7:-4]+'-'+f_row[-4:]
        df[dist["Phone"][0]].replace("--",np.nan,inplace=True)

    # Now that everything is formatted, let's see if we can populate missing primary identifiers
    if dist["Populate"] and dist["Unique"]:
        try:
            for element in dist["Unique"][1:]:
                for identity, data in df.groupby(element):
                    if identity:
                        if data[data[dist["Unique"][0]].notnull()][dist["Unique"][0]].any():
                            #print
                            df.loc[df[element] == identity,dist["Unique"][0]] = data[data[dist["Unique"][0]].notnull()][dist["Unique"][0]].sort_values(ascending=False).iloc[0]
        except:
            pass
    #return

    iterated_correct(df,corr)

    newframes = []

    if len(act_args) == 4:

        # Filter out the ones where the value of the specified field has already appeared
        df2 = df[(df.groupby(toparse).cumcount() != 0)]

        # Filter out the ones where the value of the specified field has only occurred once.
        df1 = df[(df.groupby(toparse).cumcount() == 0)]

    elif act_args[-1] == "-c":

        for value in df[toparse].unique():
            if value:
                newframes.append((df[df[toparse] == value], str(value)))
            else:
                newframes.append((df[df[toparse] == value], "Null"))

    elif act_args[-1] == "-r":
        pass

    elif act_args[-2] == "-e":

        exclude = [c.strip() for c in act_args[-1].split(',')]
        if "" not in exclude:
            newframes.append((df[~df[toparse].isin(exclude)], str(toparse) + "-not-" + "-not-".join(exclude)))
        else:
            newframes.append((df[df[toparse].notnull() & ~df[toparse].isin(exclude)], str(toparse) + "-not-" + "-not-".join(exclude)))
            
    elif len(act_args) == 3:
        newframes.append((df,outpath))
    
    else:
        raise Exception("Command invalid.")
        return

    
    # Clean and render to csv.
    if len(act_args) == 4:
        df1.to_csv(outpath + '\\' + toparse + '\\' + refmod + 'singleton.csv')
        df2.to_csv(outpath + '\\' + toparse + '\\' + refmod + 'duplicates.csv')
    
    else:
        for frame in newframes:
            frame[0].to_csv(outpath + '\\' + toparse + '\\' + frame[1] + refmod + '.csv')

def main():
    act_args = shlex.split(' '.join(sys.argv))
    # Designate distinguished fields
    dist = get_distinguished()
    # Get corrections
    corr = get_corrections()
    cleansplit(act_args,dist,corr)

if __name__ == '__main__':
    main()
