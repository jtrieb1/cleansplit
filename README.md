#CleanSplit.py

This is a python program for cleaning and categorizing .csv files in 
various ways. The behavior of this program is largely determined 
by the associated configuration files.

##CONF formats
The files **'corrections.conf'** and **'dist.conf'** have distinct 
formatting rules and consequences, as follows:

###corrections.conf
>Column Title
>
>Value to be corrected; Corrected Value
>
>Another value to be corrected; Another corrected value
>
>Another Column Title
>
>New value to be corrected; New correction

Etc, etc.

###dist.conf
>Unique; unique identifier column, another unique identifier column, 
etc
>
>Date; column in datetime format, other datetime column, etc
>
>Phone; column of phone numbers, etc
>
>Populate; columns that can have empty values populated from other 
records according to the columns in Unique

The file **dist.conf** must contain all four lines, though they can be 
blank after the initial ';'. 
